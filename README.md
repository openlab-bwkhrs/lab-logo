# Logo of the Open Source MedTec Lab at Bundeswehrkrankenhaus Hamburg (BwKhrs)

## The Logo

The repo contains the SVG source file for the logo. Please find the export formats (PNG, JPG) in the [releases](https://gitlab.com/openlab-bwkhrs/lab-logo/-/releases).

Here's the current SVG version:

![Logo-v2.0.0.svg](/uploads/3b54764b2247c4041dda4b29c46994bb/Logo-v2.0.0.svg)

- Font used: Myriad Pro (title: bold; subtitle: light)
- color: `#e30018` (color code of [Bundeswehrkrankenhaus Hamburg](https://www.bwkrankenhaus.de/), where the OpenLab is located)

## Licensor & License

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](license.md); licensor is the [New Production Institute](https://newproductioninstitute.de/en/).
